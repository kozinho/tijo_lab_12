package com.demo.springboot;

import com.demo.springboot.domain.dto.MovieDto;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ReadCsvFile {
    private List<MovieDto> movies = new ArrayList<>();
    private boolean content = false;

    public List<MovieDto> dataReading() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("./src/main/resources/movies.csv"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                MovieDto movieDto = new MovieDto(values[0], values[1], values[2], values[3]);
                if (content) {
                    movies.add(movieDto);
                } else {
                    content = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return movies;
    }
}
